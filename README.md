# Impact Normalisation

*Impact Normalisation.py* is the Python code used by REF 2021 SP11 to correct for differences in the Impact scoring behaviour of reviewers. Details of the methods used can be found in the Working Methods document in the Documents project. 

The same code was used to correct for differenes in the Environment scoring behaviour of reviewers - see the Environment Normalisation project for details.

We hope to be able to provide the real REF 2021 data in anonymised form to allow testing and experimentation, but that has not yet been agreed by the REF Team. If it is not agreed, we will consider generating comparable synthetic data.
