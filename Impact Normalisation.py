# -*- coding: utf-8 -*-
"""
Created on Thu Mar  4 14:36:13 2021

@author: Chris Taylor
"""

###############################################################################
''' 
Program to produce a ranked list of items (Impact Case Studies or Environment
Statement sections), each scored by n reviewers.

Inputs:
    Spreadsheet with two worksheets
       Score Data: list with Item ID, Panellist 1-n, P1-n Score
       Parameters: list with number of reviewers (n) and iteration limits 
       
Outputs:
    Spreadsheet with two worksheets
        Ranked List: as Score Data plus columns for average score, rank (Pos)
            Min Pos, Max Pos, Av Pos, slding window avg score (Win), corrected
            Win (Win*), sensitivity (Sens), reviewer disagreement scores
            (P1-n Wt Dis), total disagreement score (Disagree)
        Analysis: items x reviewers table plus Av Pos and P1-n Wt Dis columns
        
Notes:
    The approach involves ordering the list of items, using a bubble-sort
    mechanism, so as to maximise agreement with the partial orderings induced by
    individual reviewers' scores. Because, in general, many different global
    orderings will agree equally well with reviewers' partial orderings, the
    software quantifies the uncertainty in ranking for each item. It also
    generates a disagreement score for each of each item's reviewers, based on
    how out-of-order the global ranking of the target item was, compared to the
    reviewer’s partial ordering of items, and a guide score, based on a sliding
    window average of reviewers scores, corrected for the central tendency
    effect of averaging. See the published SP11 Working Methods document for 
    more detail.
'''
###############################################################################

import tkinter.filedialog
import numpy as np
import math as mt
import pandas as pd
import random as rand
import itertools as it
from datetime import datetime as dt

# Select an Excel file and read worksheets into Pandas DataFrame
################################################################
root = tkinter.Tk()  # define the root Tk window
filename = tkinter.filedialog.askopenfilename(parent=root)  # browse widget in root
excel_file = pd.ExcelFile(filename)
scores = pd.read_excel(excel_file, sheet_name='Score Data', index_col='Item ID')
params = pd.read_excel(excel_file, sheet_name='Parameters', index_col='Keywords')
root.destroy()  # destroy the root Tk window
# NaN values ok - ignored by mean and swaps, but remove rows with no scores (below)

# Extract parameters from params DataFrame and define keys
##########################################################
# Number of revewers to assign to each item
num_revs = int(params.loc['Reviewers', 'Values'])
# Remove rows where all reviewer scores are NaN (else mean not defined)
scores.dropna(axis='index', thresh=len(scores.columns) - num_revs + 1, inplace=True)
# Maximum number of iterations through main loop (a very large number)
max_iterations = int(params.loc['Max Iterations', 'Values'])
# Number of passes through item list without change in F before exiting
stable_passes = int(params.loc['Stable Passes', 'Values'])
# Generate list of reviewer column headings of length num_revs
rev_cols = ['Panellist ' + str(i + 1) for i in range(int(num_revs))]
# Generate list of score column headings of length num_revs
score_cols = ['P' + str(i + 1) + ' Score' for i in range(int(num_revs))]
# Generate list of  column headings of length num_revs for diagreement scores
wtdis_cols = ['P' + str(i + 1) + ' Wt Dis' for i in range(int(num_revs))]
# Extract Item ID keys
item_keys = scores.index.tolist()
#xtract reviewer keys
rev_keys = sorted(list(set(pd.concat([scores[i] for i in rev_cols]))))

# Add average score, rank and score dict columns to scores DataFrame
####################################################################
# Add column with mean of score columns (ignores NaN values)
scores['Avg'] = scores.loc[:, score_cols].mean(axis=1)
# Compute the initial rank of each item based on mean score
scores['Pos'] = scores['Avg'].rank(method='first') - 1
# Add a reviewer:score dict for each row (code split for clarity)
r = scores[rev_cols]; s = scores[score_cols]
scores['Dict'] = [dict(zip(r.loc[i].values, s.loc[i].values)) for i in item_keys]

# Main loop: iterate over item positions in ranked list, swapping if tau no worse
#################################################################################
# Zero counters for change in tau and iterations for which tau has been stable
del_tau = tau_stable = 0    
# Large number of iterations over list, until stopped by tau_stable > limit
for i in range(max_iterations):
    # Increment counter for iterations tau has been stable
    tau_stable += 1
    # If that exceeds a limit, exit the loop
    if tau_stable > stable_passes * len(scores): break
    # Current list position is i modulo number of items - 1 (ensures pos + 1 OK)
    pos = i % (len(scores) - 1)
    # Print updates
    if pos == 0: print("Delta tau: ", del_tau, "Stable: ", tau_stable)
    # Find the index for row0, currently in position pos
    row0 = scores[scores['Pos'] == pos].index[0]
    # Find the index for row1, currently in position pos + 1
    row1 = scores[scores['Pos'] == pos + 1].index[0]
    # Reset range for item at pos if this is the first pass since change in tau
    if tau_stable < len(scores):
        scores.loc[row0, 'Min'] = len(scores)
        scores.loc[row0, 'Max'] = 0
    # Otherwise update the max and min positions for this item
    else:
        scores.loc[row0, 'Max'] = scores.loc[row0, ['Max','Pos']].max()
        scores.loc[row0, 'Min'] = scores.loc[row0, ['Min','Pos']].min()
        # Also update max and min for the next row (ensures final item updated)
        scores.loc[row1, 'Max'] = scores.loc[row1, ['Max','Pos']].max()
        scores.loc[row1, 'Min'] = scores.loc[row1, ['Min','Pos']].min()
    # Extract the list of reviewers for row0
    row0_revs = list(scores.at[row0, 'Dict'].keys())
    # Extract the list of reviewers for row1
    row1_revs = list(scores.at[row1, 'Dict'].keys())
    # Find the list of reviewers present in both lists by considering all pairs
    dup = [p[0] for p in list(it.product(row0_revs, row1_revs)) if p[0] == p[1]]
    # Swapping only changes tau if rows share reviewers. If so check net effect
    if len(dup) != 0: 
        # Extract row0 and row1 dicts (for readability)
        row0_dict = scores.at[row0, 'Dict']; row1_dict = scores.at[row1, 'Dict'] 
        # Create list of reviewers in dup for which swapping would decrease tau
        better = [d for d in dup if row0_dict[d] > row1_dict[d]]
        # Create list of reviewers in dup for which swapping would increase tau
        worse = [d for d in dup if row0_dict[d] < row1_dict[d]]
        # NB NaN values ignored becuase comparisons with NaN are False
        # If the net effect of swapping would increase tau, take no action
        if len(worse) > len(better): continue
        # If swapping would improve tau, update del_tau and reset tau_stable
        if len(better) > len(worse):
            # Add the net improvement in tau to the cumulative sum
            del_tau += len(better) - len(worse)
            # Reset the number of interations for which tau has been stable
            tau_stable = 0
    # Swapping will either improve tau or leave it the same, so make the swap
    scores.loc[row0, 'Pos'] += 1
    scores.loc[row1, 'Pos'] -= 1
    
# Add columns to scores for average position and sliding window mean score
##########################################################################
# Add an average position column to scores
scores['Av Pos'] = scores[['Max', 'Min']].mean(axis=1)     
# Sort scores by Max position
scores.sort_values(by=['Max'], ascending=False, inplace=True)
# Create a running mean column for average reviewer score, padding start/end 
scores['Win'] = scores['Avg'].rolling(11, center=True).mean().fillna(scores['Avg'])    
    

# Build cumulative density function for raw and sliding window scores
#####################################################################
# Define bottom, increment and  # of bins for the histogram bins (could be params)
bot = 0; inc = 0.5; bins = 9
# Define bin centres for histograms
cents = [bot + i * inc for i in range(bins)]
# Define bin edges for histograms
edges = [bot + (i - 0.5) * inc for i in range(bins + 1)]   
# Construct histogram of raw scores
raw_hist = np.histogram(scores[score_cols], bins=edges)    
# Compute the cdf of raw scores
raw_cdf = np.cumsum(raw_hist[0]) / sum(raw_hist[0])
# Construct the histogram of sliding window scores
win_hist =  np.histogram(scores['Win'], bins=edges)   
# Compute the cdf of sliding window scores
win_cdf = np.cumsum(win_hist[0]) / sum(win_hist[0])

# Compute a corrected sliding window score and sensitivity for each item
######################################################################## 
# Loop over ICSs
for i in item_keys:
    # Correct sliding window score so cdf of corrected scores = cdf of raw scores
    #############################################################################
    # From Win, find integer index into sliding window cdf
    idx = int(np.floor((1 / inc) * (scores.loc[i, 'Win'] - bot)))
    # Define next index ensuring it is within limits
    nxt_idx = np.clip(idx + 1, bot, (bins - 1))
    # Find the fractional remainder
    fract = ( 1 / inc) * scores.loc[i, 'Win'] - idx
    # Calculate interpolated area under cdf up to Win value of current item
    area_lt = win_cdf[idx] + fract*(win_cdf[nxt_idx] - win_cdf[idx])   
    # Find index of first value in raw score cdf that exceeds that area
    idx = min([k for k,v in enumerate(raw_cdf) if v >= area_lt])
    # Define previous index ensuring it is within limits
    pre_idx = np.clip(idx -1, bot, bins - 1)
    # Calculate fraction for interpolating raw score cdf, avoiding div0
    if idx==pre_idx: fract = 0
    else: fract = (area_lt - raw_cdf[pre_idx]) / (raw_cdf[idx] - raw_cdf[pre_idx])
    # Save sliding window score corrected for central tendency
    scores.loc[i, 'Win*'] = cents[pre_idx] + fract * (cents[idx] - cents[pre_idx])   
    # Compute sensitivity as the number of items that could have been either side
    #############################################################################
    # Extract the position for this item
    pos = scores.loc[i, 'Pos']       
    # Find number of item ranges that pos lies within
    s = len([k for k in item_keys if scores.loc[k,'Min'] <= pos < scores.loc[k, 'Max']])
    # Record as sensitivity for this item
    scores.loc[i, 'Sens'] = s   
      
# Generate a disagreement score for each reviewer and a combined score for each item
####################################################################################
# Create Series of dicts indexed by reviewer
rev_ps = pd.Series(index=rev_keys, data=dict)
# Create a position:score dict for each reviewer
for r in rev_keys:
    # Extract list of score rows that include reviewer r, ordered by position
    rlist = scores[scores.values == r].sort_values('Av Pos')
    # Create a postion:score dict for this reviewer
    rev_ps[r] = dict(zip(list(rlist['Av Pos'].values), [d[r] for d in rlist['Dict']]))
# Loop over Items and count of out-of-order scores weighted by position difference
for i in item_keys:
    # Set total disagreement to zero
    disagree = 0
    # Extract position for current item
    pos = scores.at[i, 'Av Pos']
    # Loop over all reviewer:score pairs for the current item 
    for r, s in scores.at[i, 'Dict'].items():
        # Sum Av Pos difference for position:score pairs where Av Pos > pos but score < s
        wtabove = sum([k - pos for (k, v) in rev_ps[r].items() if (k > pos) and (v < s)])
        # Sum Av Pos difference for position:score pairs where Av Pos < pos but score > s
        wtbelow = sum([pos - k for (k, v) in rev_ps[r].items() if (k < pos) and (v > s)])
        # Calculate total weighted discrepancy for this reviewer
        wtdis = (wtabove - wtbelow) / len(rev_ps[r])
        # Save in the appropriate column in the scores dataframe
        scores.loc[i, 'P' + str(list(scores.at[i, 'Dict']).index(r) + 1) + ' Wt Dis'] = wtdis
        # Increment total disagreement score
        disagree += abs(wtdis)
    # Save aggregate disagreement score for this item
    scores.loc[i, 'Disagree'] = disagree

# Generate an items x revs diagnostic table with scores + Av Pos and Wt Dis columns  
###################################################################################
# Create a zeroed item_keys x rev_keys DataFrame 
diag = pd.DataFrame(index=item_keys, columns=rev_keys, data=0)
# Add average position and disagreement columns copied from the scores DataFrame
diag[['Av Pos'] + wtdis_cols] = scores[['Av Pos'] + wtdis_cols]
# Loop through items
for i in item_keys:
    # Enter the score for each reviewer in the DataFrame
    for r in scores.loc[i, 'Dict']: diag.loc[i, r] = scores.loc[i, 'Dict'][r]

# Output the results to a spreadsheet
#####################################    
# Construct output filename including date and time
now = dt.now()
part = filename.rpartition('/')  # extract the path by splitting at rightmost /
fileout = part[0] + '/Normalisation Result ' + now.strftime("%Y-%m-%d(%H%M)") + '.xlsx'
# Output to spreadsheet
with pd.ExcelWriter(fileout) as writer:
    scores.to_excel(writer, sheet_name='Ranked List')
    diag.to_excel(writer, sheet_name='Analysis')







